
const eyeIcons = document.querySelectorAll('i');
const inputs = document.querySelectorAll('input');
const slashedIcons = document.querySelectorAll('i.fa-eye-slash');
const button = document.querySelector('button');

for(let i = 0; i < eyeIcons.length; i++) {
    
        
    eyeIcons[i].addEventListener('click', (e) => {
        const currentInput = e.target.previousSibling.previousSibling;
        const currentIcon = e.target.nextSibling.nextSibling;

        
        if(currentInput.hasAttribute('type')){
        currentInput.removeAttribute('type')
        currentIcon.classList.remove('invisible')
        }
        else {
        currentInput.previousSibling.previousSibling.setAttribute('type', 'password');
        e.target.classList.add('invisible')
        }

       
    })
}


button.addEventListener('click', (e) => {
    e.preventDefault
    if(inputs[0].value === inputs[1].value) {
        alert("You are welcome")
    }
    else {
        let errorText = document.createElement('span');
        errorText.textContent = 'Потрібно ввести однакові значення';
        errorText.style.color = 'red';
        inputs[1].insertAdjacentElement("afterend", errorText)
    }
})
